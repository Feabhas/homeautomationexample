#pragma once
#include <cstdint>

class Time
{
public:
	Time(uint8_t hours, uint8_t minutes, uint8_t seconds );
	bool isEqual(const Time& t);
private:
	uint8_t tm_sec;
	uint8_t tm_min;
	uint8_t tm_hour;
};