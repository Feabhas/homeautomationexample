#pragma once
#include "lamp.h"
class NamedLamp :
	public Lamp
{
public:
	NamedLamp(const char* lmapName, char hc, int dc);
	~NamedLamp();

	void on();
	void off();
private:
	char* name;
};

