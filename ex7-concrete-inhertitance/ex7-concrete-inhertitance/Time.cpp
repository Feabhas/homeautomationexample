#include "Time.h"

Time::Time(uint8_t hours, uint8_t minutes, uint8_t seconds ):
	tm_sec(seconds),
	tm_min(minutes),
	tm_hour(hours)
{
	if(tm_sec > 59) tm_sec = 0;
	if(tm_min > 59) tm_min = 0;
	if(tm_hour > 23) tm_hour = 0;
}

bool Time::isEqual(const Time& t)
{
	return ( (this->tm_hour == t.tm_hour) &&
		(this->tm_min == t.tm_min) &&
		(this->tm_sec == t.tm_sec) );
}