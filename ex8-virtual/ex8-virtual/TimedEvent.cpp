#include "TimedEvent.h"
#include "Room.h"

TimedEvent::TimedEvent(	Time on_t, Time off_t, Room* r):
	onTime(on_t),
	offTime(off_t),
	room(r) 
{
}

void TimedEvent::doAction(const Time& now)
{
	if(onTime.isEqual(now)) {
		room->allLightsOn();
	}
	if(offTime.isEqual(now)) {
		room->allLightsOff();
	}
}