#pragma once
#include "lamp.h"
class NamedLamp :
	public Lamp
{
public:
	NamedLamp(const char* lmapName, char hc, int dc);
	~NamedLamp();

	virtual void on();
	virtual void off();
private:
	char* name;
};

