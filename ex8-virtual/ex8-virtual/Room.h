#pragma once

class Lamp;

class Room
{
public:
	Room(const char *);
	~Room();

	void allLightsOn();
	void allLightsOff();
	void displayStatus();
	bool addLamp(Lamp* lamp);
public:
	char* roomName;
	Lamp* lamps[4];

};

