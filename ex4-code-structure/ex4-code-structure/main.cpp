#include <iostream>
#include "Lamp.h"

int main()
{
	Lamp studyLamp('A', 1);

	studyLamp.off();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;

	}
	studyLamp.on();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;

	}

#ifdef _WIN32
	system("PAUSE");
#endif
}
