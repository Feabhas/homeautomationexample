#include <iostream>
#include <cmath>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setiosflags;
using std::setprecision;
using std::ios;
using std::setw;


int main()
{
  const unsigned int numValues = 10;
  int values[numValues];
  double total = 0.0;
  
  cout << "Enter 10 integer values:" << endl;
  
  for(int i = 0; i < numValues; ++i)
  {
    cout << "Value " << setw(2) << (i + 1) << ": ";
    cin >> values[i];
    total += static_cast<double>(values[i]);
  }
  
  cout << endl << "You entered:" << endl;
  for(int i = 0; i < numValues; ++i)
  {
    cout << "Value " << setw(2) << (i + 1) << ": ";
    cout << setw(3) << values[i] << endl; 
  }
  
  cout << "The average was ";
  cout << setiosflags(ios::fixed);
  cout << setprecision(2);
  cout << total/numValues << endl;
}
