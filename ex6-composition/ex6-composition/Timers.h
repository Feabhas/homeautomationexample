#pragma once

#include "Time.h"

class TimedEvent;
class Room;


class Timers
{
public:
	Timers();
	~Timers();
	bool createEvent(Room& r, Time onTime, Time offTime);
	void updateTime(const Time& now);
private:
	TimedEvent* eventList[16];
};

