#pragma once
#include "Time.h"

class Room;

class TimedEvent
{
public:
	TimedEvent(	Time on_t, Time off_t, Room* r);
	void doAction(const Time& now);
private:
	Time onTime;
	Time offTime;
	Room* room;
};
