#include "Lamp.h"
#include "Room.h"
#include "TimedEvent.h"
#include "Time.h"

#include <cstdlib>

int main()
{
	Lamp studyLamp1('A', 1);
	Lamp studyLamp2('A', 2);

	Room study("Study");

	study.addLamp(&studyLamp1);
	study.addLamp(&studyLamp2);

	Time t1( 6, 30, 0);
	Time t2( 9,  0, 0);

	TimedEvent morning(t1, t2, &study);
 
	study.displayStatus();

	morning.doAction(t1);
	study.displayStatus();

	morning.doAction(t2);
	study.displayStatus();

#ifdef _WIN32
	system("PAUSE");
#endif
}