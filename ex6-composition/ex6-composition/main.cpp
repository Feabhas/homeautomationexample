#include "Lamp.h"
#include "Room.h"
#include "Timers.h"
#include "Time.h"

#include <cstdlib>

int main()
{
	Lamp studyLamp1('A', 1);
	Lamp studyLamp2('A', 2);

	Room study("Study");

	study.addLamp(&studyLamp1);
	study.addLamp(&studyLamp2);


	Timers homeAutomation;

	Time t1( 6, 30, 0);
	Time t2( 9,  0, 0);
	Time t3(17, 30, 0);
	Time t4(22, 30, 0);

	homeAutomation.createEvent(study, t1, t2);
	homeAutomation.createEvent(study, t3, t4);

	study.displayStatus();

	homeAutomation.updateTime(t1);
	study.displayStatus();
	homeAutomation.updateTime(t2);
	study.displayStatus();
	homeAutomation.updateTime(t3);
	study.displayStatus();
	homeAutomation.updateTime(t4);
	study.displayStatus();

#ifdef _WIN32
	system("PAUSE");
#endif
}