#include "Room.h"
#include "Lamp.h"

#include <cstring>
#include <iostream>

Room::Room(const char * name)
{
	roomName = new char[strlen(name)+1];
	strcpy(roomName, name);
	memset(lamps, 0, sizeof(Lamp*)*4);
}


Room::~Room(void)
{
	allLightsOff();
}

void Room::allLightsOn()
{
	for(unsigned i = 0; i < 4; ++i)
	{
		if(lamps[i] != 0) {
			lamps[i]->on();
		}
	}
}

void Room::allLightsOff()
{
	for(unsigned i = 0; i < 4; ++i)
	{
		if(lamps[i] != 0) {
			lamps[i]->off();
		}
	}
}

void Room::displayStatus()
{
	int lampsOn = 0;
	int lampsOff = 0;
	for(unsigned i = 0; i < 4; ++i)
	{
		if(lamps[i] != 0) {
			if(lamps[i]->isOn()) {
				lampsOn++;
			}
			else {
				lampsOff++;
			}
		}
	}
	std::cout << "In the " << roomName << ", there are current " << lampsOn << " lamps on and " << lampsOff << " lamps off." << std::endl;
}

bool Room::addLamp(Lamp* lamp)
{
	for(unsigned i = 0; i < 4; ++i)
	{
		if(lamps[i] == 0) {
			lamps[i] = lamp;
			return true;
		}
	}
	return false;
}