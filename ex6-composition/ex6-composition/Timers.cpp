#include "Timers.h"
#include "Room.h"
#include "TimedEvent.h"
#include <cstring>


Timers::Timers()
{
	memset(eventList, 0, sizeof(TimedEvent*)*16);
}


Timers::~Timers()
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] != 0) {
			delete eventList[i];
		}
	}
}

bool Timers::createEvent(Room& r, Time onTime, Time offTime)
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] == 0) {
			TimedEvent* p = new TimedEvent(onTime, offTime, &r);
			eventList[i] = p ;
			return true;
		}
	}
	return false;
}

void Timers::updateTime(const Time& now)
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] != 0) {
			eventList[i]->doAction(now);
		}
	}
}