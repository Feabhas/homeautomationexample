#include "Lamp.h"
#include <iostream>

void Lamp::on()
{
	status = true;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned on" << std::endl;
}

void Lamp::off()
{
	status = false;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned off" << std::endl;
}

bool Lamp::isOn()
{
	return status;
}

Lamp::Lamp(char hc, int dc):houseCode(hc), deviceCode(dc)
{
	off();
}

Lamp::~Lamp()
{
	off();
}
