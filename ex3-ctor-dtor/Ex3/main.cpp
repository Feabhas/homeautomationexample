#include <iostream>


class Lamp
{
public:
	Lamp(char hc, int dc);
	~Lamp();

	void on();
	void off();
	bool isOn();

private:
	char houseCode;
	int deviceCode;
	bool status;
};

void Lamp::on()
{
	status = true;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned on" << std::endl;
}

void Lamp::off()
{
	status = false;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned off" << std::endl;
}

bool Lamp::isOn()
{
	return status;
}

Lamp::Lamp(char hc, int dc):houseCode(hc), deviceCode(dc)
{
	off();
}

Lamp::~Lamp()
{
	off();
}

int main()
{
	Lamp studyLamp('A', 1);

	studyLamp.off();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;

	}
	studyLamp.on();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;

	}

#ifdef _WIN32
	system("PAUSE");
#endif
}
