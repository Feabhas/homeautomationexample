#pragma once

class Lamp
{
public:
	Lamp(char hc, int dc);
	~Lamp();

	void on();
	void off();
	bool isOn();

private:
	char houseCode;
	int deviceCode;
	bool status;
};
