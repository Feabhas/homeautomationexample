#include "Lamp.h"
#include "Room.h"
#include <cstdlib>

int main()
{
	Lamp studyLamp1('A', 1);
	Lamp studyLamp2('A', 2);

	Room study("Study");

	study.addLamp(studyLamp1);
	study.addLamp(studyLamp2);

	study.displayStatus();

	study.allLightsOn();
	study.displayStatus();

	study.allLightsOff();
	study.displayStatus();

#ifdef _WIN32
	system("PAUSE");
#endif
}