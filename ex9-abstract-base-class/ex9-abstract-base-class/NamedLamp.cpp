#include "NamedLamp.h"
#include <cstring>
#include <iostream>

NamedLamp::NamedLamp(const char* lampName,char hc, int dc):Lamp(hc,dc)
{
	name = new char[strlen(lampName)+1];
	strcpy(name, lampName);
}


NamedLamp::~NamedLamp()
{
}

void NamedLamp::on()
{
	std::cout << name << ": ";
	Lamp::on();
}

void NamedLamp::off()
{
	std::cout << name << ": ";
	Lamp::off();
}