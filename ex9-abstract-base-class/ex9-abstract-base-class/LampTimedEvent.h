#pragma once
#include "timedevent.h"

class Lamp;

class LampTimedEvent :
	public TimedEvent
{
public:
	LampTimedEvent(Lamp& theLamp, Time on_t, Time off_t);
	virtual ~LampTimedEvent();
	virtual void doAction(const Time& now);
private:
	Lamp& lamp;
};
