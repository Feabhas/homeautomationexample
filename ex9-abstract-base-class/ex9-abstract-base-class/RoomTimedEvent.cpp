#include "RoomTimedEvent.h"
#include "Room.h"


RoomTimedEvent::RoomTimedEvent(Room& theRoom, Time on_t, Time off_t):
	TimedEvent(on_t, off_t), room(theRoom)
{
}


RoomTimedEvent::~RoomTimedEvent(void)
{
}

void RoomTimedEvent::doAction(const Time& now)
{
	if(onTime.isEqual(now)) {
		room.allLightsOn();
	}
	if(offTime.isEqual(now)) {
		room.allLightsOff();
	}
}