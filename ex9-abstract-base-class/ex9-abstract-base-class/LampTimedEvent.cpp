#include "LampTimedEvent.h"
#include "Lamp.h"

LampTimedEvent::LampTimedEvent(Lamp& theLamp, Time on_t, Time off_t):TimedEvent(on_t, off_t), lamp(theLamp)
{
}


LampTimedEvent::~LampTimedEvent()
{
}


void LampTimedEvent::doAction(const Time& now)
{
	if(onTime.isEqual(now)) {
		lamp.on();
	}
	if(offTime.isEqual(now)) {
		lamp.off();
	}
}