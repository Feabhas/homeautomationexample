#pragma once
#include "Time.h"


class TimedEvent
{
public:
	TimedEvent(	Time on_t, Time off_t);
	virtual ~TimedEvent(){}
	virtual void doAction(const Time& now) = 0;
protected:
	Time onTime;
	Time offTime;
};
