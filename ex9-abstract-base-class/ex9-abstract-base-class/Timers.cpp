#include "Timers.h"
#include "TimedEvent.h"
#include <cstring>

#include "RoomTimedEvent.h"
#include "LampTimedEvent.h"

Timers::Timers()
{
	memset(eventList, 0, sizeof(TimedEvent*)*16);
}


Timers::~Timers()
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] != 0) {
			delete eventList[i];
		}
	}
}

bool Timers::createEvent(Room& room, Time onTime, Time offTime)
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] == 0) {
			TimedEvent* p = new RoomTimedEvent(room, onTime, offTime);
			eventList[i] = p ;
			return true;
		}
	}
	return false;
}

bool Timers::createEvent(Lamp& lamp, Time onTime, Time offTime)
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] == 0) {
			TimedEvent* p = new LampTimedEvent(lamp, onTime, offTime);
			eventList[i] = p ;
			return true;
		}
	}
	return false;
}
void Timers::updateTime(const Time& now)
{
	for(unsigned i = 0; i < 16; i++) {
		if(eventList[i] != 0) {
			eventList[i]->doAction(now);
		}
	}
}