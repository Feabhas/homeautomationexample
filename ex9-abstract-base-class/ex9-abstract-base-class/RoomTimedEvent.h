#pragma once
#include "timedevent.h"

class Room;

class RoomTimedEvent :
	public TimedEvent
{
public:
	RoomTimedEvent(Room& theRoom, Time on_t, Time off_t);
	virtual ~RoomTimedEvent();
	virtual void doAction(const Time& now);
private:
	Room& room;
};

