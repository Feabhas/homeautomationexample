#include <iostream>

class Lamp
{
public:
	void on();
	void off();
	bool isOn();
	void setCode(char hc, int dc);
private:
	char houseCode;
	int deviceCode;
	bool status;
};

void Lamp::on()
{
	status = true;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned on" << std::endl;
}

void Lamp::off()
{
	status = false;
	std::cout << "Lamp(" << houseCode << ',' << deviceCode << ") has been turned off" << std::endl;
}

bool Lamp::isOn()
{
	return status;
}

void Lamp::setCode(char hc, int dc)
{
	houseCode = hc;
	deviceCode = dc;
}


int main()
{
	Lamp studyLamp;
	studyLamp.setCode('A', 1);

	studyLamp.off();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;

	}
	studyLamp.on();

	std::cout << "The Lamp is currently ";
	if(studyLamp.isOn()){
		std::cout << "on" << std::endl;
	}
	else {
		std::cout << "off" << std::endl;
	}

#ifdef _WIN32
	system("PAUSE");
#endif
}
